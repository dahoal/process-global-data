# david hoffmann, mcccrh, march 2022

library(tidyverse)
library(lubridate)
library(here)
library(magrittr)

#' Reads GHG Emissions data from Our World In Data on GitHub
#' (https://github.com/owid/co2-data). All columns are described in the cookbook
#' available in the same GitHub repository. This script adds a column for both
#' southern and northern hemisphereto work out their respective contributions to
#' the global GHG emissions.

nh <- c("Afghanistan", "Albania", "Algeria", "Andorra", "Anguilla",
    "Antigua and Barbuda", "Armenia", "Aruba", "Austria", "Azerbaijan",
    "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belize", "Bolivia",
    "Bonaire Sint Eustatius and Saba", "British Virgin Islands", "Belarus",
    "Belgium", "Benin", "Bermuda", "Bhutan", "Bosnia and Herzegovina",
    "Bulgaria", "Burkina Faso", "Brunei", "Cape Verde", "Colombia",
    "Cambodia", "Cameroon", "Canada", "Central African Republic", "Chad",
    "China", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Curacao",
    "Cyprus", "Czechia", "Denmark", "Djibouti", "Dominica",
    "Dominican Republic", "Egypt", "El Salvador", "Equatorial Guinea",
    "Eritrea", "Estonia", "Ethiopia", "Faeroe Islands", "Finland", "France",
    "French Guiana", "Gambia", "Georgia", "Germany", "Ghana", "Greece",
    "Greenland", "Grenada", "Guadeloupe", "Guatemala", "Guinea",
    "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary",
    "Iceland", "India", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica",
    "Japan", "Jordan", "Kazakhstan", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos",
    "Latvia", "Lebanon", "Leeward Islands", "Liberia", "Libya", "Liechtenstein",
    "Lithuania", "Luxembourg", "Macao", "Malaysia", "Maldives", "Mali", "Malta",
    "Marshall Islands", "Martinique", "Mauritania", "Mexico",
    "Micronesia (country)", "Moldova", "Mongolia", "Montenegro", "Montserrat",
    "Morocco", "Myanmar", "Nepal", "Netherlands", "Nicaragua", "Niger",
    "Nigeria", "North Korea", "North Macedonia", "Norway", "Oman", "Pakistan",
    "Palau", "Palestine", "Panama", "Philippines", "Poland", "Portugal",
    "Puerto Rico", "Qatar", "Romania", "Russia", "Saint Kitts and Nevis",
    "Saint Lucia", "Saint Pierre and Miquelon",
    "Saint Vincent and the Grenadines", "Sao Tome and Principe", "Saudi Arabia",
    "Senegal", "Serbia", "Sierra Leone", "Singapore",
    "Sint Maarten (Dutch part)", "Slovakia", "Slovenia", "Somalia",
    "South Korea", "South Sudan", "Spain", "Sri Lanka",
    "St. Kitts-Nevis-Anguilla", "Sudan", "Suriname", "Sweden", "Switzerland",
    "Syria", "Taiwan", "Tajikistan", "Thailand", "Togo", "Trinidad and Tobago",
    "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Ukraine",
    "United Arab Emirates", "United Kingdom", "United States", "Uzbekistan",
    "Venezuela", "Vietnam", "Yemen")

sh <- c("Angola", "Antarctica", "Argentina", "Australia", "Botswana", "Brazil",
    "Burundi", "Chile", "Christmas Island", "Comoros", "Congo", "Cook Islands",
    "Democratic Republic of Congo", "Ecuador", "Eswatini", "Fiji",
    "French Polynesia", "Gabon", "Indonesia", "Kenya", "Kiribati", "Lesotho",
    "Madagascar", "Malawi", "Mauritius", "Mayotte", "Mozambique", "Namibia",
    "New Caledonia", "New Zealand", "Nauru", "Niue", "Papua New Guinea",
    "Paraguay", "Peru", "Reunion", "Rwanda", "Saint Helena", "Samoa",
    "Seychelles", "Solomon Islands", "South Africa", "Tanzania", "Timor",
    "Tonga", "Tuvalu", "Uganda", "Uruguay", "Vanuatu", "Wallis and Futuna",
    "Zambia", "Zimbabwe")

partial <- c("Brazil", "Colombia", "Congo", "Democratic Republic of Congo",
    "Ecuador", "Gabon", "Indonesia", "Kenya", "Kiribati", "Maldives",
    "Sao Tome and Principe", "Somalia", "Uganda")

col_select <- c("iso_code", "country", "year", "co2", "trade_co2",
    "co2_per_capita", "cumulative_co2", "total_ghg", "ghg_per_capita",
    "population", "total_ghg_excluding_lucf")


# Read data from GitHub to have most up to date data and convert to tibble
raw <-
    as_tibble(
        read.csv("https://raw.githubusercontent.com/owid/co2-data/master/owid-co2-data.csv")) %>% # nolint
    select(all_of(col_select)) %>%
    mutate(
        hemisphere = case_when(
            country %in% nh      ~ "NH",
            country %in% sh      ~ "SH") #,
            # Overwrite those countries that are intersected by the equator
            #country %in% partial ~ "partial")
        ) %>%
    filter(!is.na(hemisphere)) %>%
    filter(year == c("2018"))

result <-
    raw %>%
    group_by(hemisphere) %>%
    summarise(Co2            = sum(co2, na.rm = TRUE),
              trade_Co2      = sum(trade_co2, na.rm = TRUE),
              total_GHG      = sum(total_ghg, na.rm = TRUE),
              total_ghg_excl_lucf = sum(total_ghg_excluding_lucf, na.rm = TRUE),
              all_countries  = length(country),
              Co2_miss       = length(which(is.na(co2))),
              total_GHG_miss = length(which(is.na(total_ghg))),
              total_ghg_excl_lucf_miss = length(which(is.na(total_ghg_excluding_lucf))))

missing_cnty_co2 <-
    raw %>%
    filter(is.na(co2)) %>%
    select(country)
missing_cnty_co2$country

missing_cnty_ghg <-
    raw %>%
    filter(is.na(total_ghg)) %>%
    select(country)
missing_cnty_ghg$country

missing_cnty_ghg_excl_lucf <-
    raw %>%
    filter(is.na(total_ghg_excluding_lucf)) %>%
    select(country)
missing_cnty_ghg_excl_lucf$country















