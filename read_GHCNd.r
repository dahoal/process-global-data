library(tidyverse)
library(here)
library(lubridate)
install.packages("filesstrings")
library(filesstrings)

min_name <- "IDCJAC0011_999999_1800"
max_name <- "IDCJAC0010_999999_1800"

##### Reading GHCN data #####

# List all files to be manipulated
lst_in <- list.files(here("in"), pattern = glob2rx("GHCN*.csv"))

# read data
raw <-
  read_csv(
    paste(here("in/"), lst_in, sep = ""),
    col_select = c("STATION", "NAME", "DATE", "TMAX", "TMIN"))


raw_obs <-
  raw %>%
  # Complete time series
  complete(DATE = seq.Date(min(DATE), max(DATE), by = "day"), STATION) %>%
  #mutate(STATION = "999999") %>% #substr(STATION, 1, 6)) %>%
  # Add columns for year, month, day, and other to immitate BOM structure
  mutate(Year = format(DATE, "%Y")) %>%
  mutate(Month = format(DATE, "%m")) %>%
  mutate(Day = format(DATE, "%d")) %>%
  mutate("Days of accumulation" = NA, check.names = FALSE) %>%
  mutate(Quality = NA) %>%
  # Manipulate data: set NAs and convert from F to C
  mutate(across(c("TMAX", "TMIN"),
            ~ replace(.x, .x == 9999, NA))) %>%
  mutate(across(c("TMAX", "TMIN"), ~.x / 10)) %>%
  rename(., `Maximum temperature (Degree C)` = TMAX) %>%
  rename(., `Minimum temperature (Degree C)` = TMIN)

# Check and plot available data for each year
rec_length_y <-
  raw_obs %>%
  #mutate(DATE = as.Date(raw_obs$DATE)) %>%
  group_by(Year) %>% 
  summarise(n_days_present_max = length(which(!is.na(`Maximum temperature (Degree C)`))),
            n_days_present_min = length(which(!is.na(`Minimum temperature (Degree C)`))),
            all_days = n()) %>%
  mutate(prct_present_max = n_days_present_max / all_days * 100) %>%
  mutate(prct_present_min = n_days_present_min / all_days * 100)

plt <- ggplot() +
  geom_point(data = rec_length_y, aes(x = as.numeric(Year),
                                      y = n_days_present_max, color = "red")) +
  geom_point(data = rec_length_y, aes(x = as.numeric(Year),
                                      y = n_days_present_min, colour = 'blue')) +
  geom_hline(yintercept = 365 * 0.95, linetype = "dashed", color = "red") +
  ylab("Days present / year") +
  scale_x_continuous(breaks = seq(min(rec_length_y$Year),
                                  max(rec_length_y$Year), by = 5)) +
  scale_color_manual(
    values = c("red", "blue"),
    labels = expression("max", "min"))

plt

# Create file for minimum temperature
csv_file <- paste0(here("out/"), min_name, "_Data.csv")

min <-
  raw_obs %>%
  mutate("Product code" = paste0(substr(lst_in, 1, 9), "1"), check.names = FALSE) %>%
  # select columns according to BOM structure
  select(`Product code`, STATION, Year, Month, Day,
        `Minimum temperature (Degree C)`, `Days of accumulation`, Quality)

# Check output
head(min)

# Write csv file
write.csv(min, csv_file, row.names = FALSE)
zip_file <- list.files(here("out/"), pattern = glob2rx("IDCJAC001*_1800_Data.csv"))
file.move(csv_file, here())

# Zip csv
zip(paste0("out/", min_name), paste0(zip_file))
file.remove(paste0(here(), "/", min_name, "_Data.csv"))

# Create file for maximum temperature
csv_file <- paste0(here("out/"), max_name, "_Data.csv")

max <-
  raw_obs %>%
  mutate("Product code" = paste0(substr(lst_in, 1, 9), "1"), check.names = FALSE) %>%
  # select columns according to BOM structure
  select(`Product code`, STATION, Year, Month, Day,
        `Maximum temperature (Degree C)`, `Days of accumulation`, Quality)

# Check output
head(max)

# Write csv file
write.csv(max, csv_file, row.names = FALSE)
zip_file <- list.files(here("out/"), pattern = glob2rx("IDCJAC001*_1800_Data.csv"))
file.move(csv_file, here())

# Zip csv
zip(paste0("out/", max_name), paste0(zip_file))
file.remove(paste0(here(), "/", max_name, "_Data.csv"))