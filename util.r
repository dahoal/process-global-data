# david hoffmann, mcccrh, march 2022

library(tidyverse)
library(lubridate)
library(here)

#' Reads csv selected in GSOD-to-BYOD.rmd file and determines whether GSOD or
#' GHCND data was selected. Then it continues with filling missing time steps
#' and converting temperature to Degrees Celcius. The function finishes with
#' renaming and rearranging the columns according to typical BOM data but has
#' maximum AND minimum data.
#' @param df The dataframe read from the csv input file
#' @param station Six digit station number as a BOM station. Default is set to
#'   999999 to avoid coincidentally naming the same station as a BOM one, which
#'   could lead to the addition of ACORN SAT data and thus wrong time series.
read_dataset <- function (df, station = "999999") {

    coln <- colnames(df)
    if (all(c("TMAX", "TMIN") %in% coln)) {
        message("Reading GHCN data.")
        # Specify names and missing value
        tmax <- "TMAX"
        tmin <- "TMIN"
        miss <- 9999
        # Convert temperture to tenth of degree C (i.e. 236 to 23.6)
        converttemp <- function(data) {
            data / 10
        }
        message("Station name: ", raw$NAME[1])
        message("GHCND station number: ", raw$STATION[1])

    } else if (all(c("MAX", "MIN") %in% coln)) {
        message("Reading GSOD data.")
        # Specify names and missing value
        tmax <- "MAX"
        tmin <- "MIN"
        miss <- 9999.9
        # Convert temperture from Fahrenheit to Celsius
        converttemp <- function(data) {
            (data - 32) * 5 / 9
        }
        message("Station name: ", raw$NAME[1],
                "\nGSOD station number: ", substr(raw$STATION[1], 1, 6))
    } else {
        stop("Dataset not recognised. Select either GSOD or GHCND csv file.")
    }

    out <-
        df %>%
        # Complete time series
        mutate(DATE = as.Date(DATE)) %>%
        complete(DATE = seq.Date(min(DATE), max(DATE), by = "day"), STATION) %>%
        mutate(STATION = station) %>% #substr(STATION, 1, 6)) %>%
        # Add columns for year, month, day, and other to immitate BOM structure
        mutate(Year = format(DATE, "%Y")) %>%
        mutate(Month = format(DATE, "%m")) %>%
        mutate(Day = format(DATE, "%d")) %>%
        mutate("Days of accumulation" = NA, check.names = FALSE) %>%
        mutate(Quality = NA) %>%
        # Manipulate data: set NAs and convert from F to C
        mutate(across(c(tmax, tmin),
                    ~ replace(.x, .x == miss, NA))) %>%
        mutate(across(c(tmax, tmin),
                    ~ converttemp(.x))) %>%
        rename(., `Maximum temperature (Degree C)` = tmax) %>%
        rename(., `Minimum temperature (Degree C)` = tmin)

    return(out)
}

#' Returns the number of available data, that is not NA, as absolute number of
#' days per year and as a percentage.
#' @param data A dataframe as return from the read_dataset function or any con-
#' ventional BOM minumum and mximum data.
avail_data_year <- function(data = df) {
    rec_length <-
        data %>%
        group_by(Year) %>%
        # Count days with data available
        summarise(n_days_present_max = length(which(!is.na(`Maximum temperature (Degree C)`))), # nolint
            n_days_present_min = length(which(!is.na(`Minimum temperature (Degree C)`))), # nolint
            all_days = n()) %>%
        # Calculate percentage of present data
        mutate(prct_present_max = n_days_present_max / all_days * 100) %>%
        mutate(prct_present_max = n_days_present_max / all_days * 100)

    return(rec_length)
}

#' Returns a plot of available data per year
#' @param df Dataframe as returned from avail_data_year function
plot_avail_data <- function(df) {
    plt <- ggplot() +
        geom_point(data = df, aes(x = as.numeric(Year),
                                  y = n_days_present_max, color = "red")) +
        geom_point(data = df, aes(x = as.numeric(Year),
                                  y = n_days_present_min, colour = "blue")) +
        geom_hline(yintercept = 365 * 0.95,
                   linetype = "dashed",
                   color = "red") +
        ylab("Days present / year") +
        scale_x_continuous(breaks = seq(min(df$Year),
                                        max(df$Year), by = 5)) +
            scale_color_manual(
        values = c("red", "blue"),
        labels = expression("max", "min"))
    return(plt)
}

#' Creates a final output file for maximum and minimum temperature respectively
#' that is idnetical in it's structure to BOM max and min station data.
#' @param data The dataframe containing both max and min temperature as per
#'   output from read_dataset function
#' @param var "tmax" for daily maximum temperatures; "tmin" for daily minima
out_df <- function(data, var = c("tmax", "tmin")) {
    var <- match.arg(var)
    product_code <- recode(var,
        "tmax" = params$max_name,
        "tmin" = params$min_name)

    if (var == "tmax") {
        delivery_col <- c("Product code", "STATION", "Year", "Month", "Day",
            "Maximum temperature (Degree C)", "Days of accumulation",
            "Quality")
    } else if (var == "tmin") {
        delivery_col <- c("Product code", "STATION", "Year", "Month", "Day",
            "Minimum temperature (Degree C)", "Days of accumulation",
            "Quality")
    }

    out <-
        data %>%
        mutate("Product code" = substr(product_code, 1, 10),
                                        check.names = FALSE) %>%
    # select columns according to BOM structure
        select(any_of(delivery_col))

    return(out)
}

#' Writes the final output file as csv for maximum and minimum temperature re-
#' spectively, compresses it as zip and removes csv file, if specifed.
#' @param data The dataframe to be written and compressed as zip file. Meant for
#'   output of out_df function.
#' @param var "tmax" for daily maximum temperatures; "tmin" for daily minima
#' @param delcsv Specifies if csv file written should be deleted after compres-
#'   sion. Default is TRUE.
write_output <- function(data, var = c("tmax", "tmin"), delcsv = TRUE) {
    var <- match.arg(var)
    product_code <- recode(var,
        "tmax" = params$max_name,
        "tmin" = params$min_name)
    csv_file <- paste0(here("out/"), product_code, "_Data.csv")

    write.csv(data, csv_file, row.names = FALSE)
    zip_file <- list.files(here("out/"), pattern = glob2rx("IDCJAC001*_1800_Data.csv"))
    file.move(csv_file, here())

    # Zip csv and remove
    zip(paste0("out/", product_code), paste0(zip_file))
    if (delcsv) {
        invisible(file.remove(paste0(here(), "/", product_code, "_Data.csv")))
    }
}